package com.raihanorium.collab.server.repository;

import com.raihanorium.collab.server.model.Group;
import com.raihanorium.collab.server.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * @author ataul.raihan
 * @date 10/21/2017
 */
public interface GroupRepository extends JpaRepository<Group, Long> {
    public Page<Group> findAllByEnabled(boolean enabled, Pageable pageable);

    public Page<Group> findAllByEnabledAndAdminsContains(boolean enabled, Set<User> admins, Pageable pageable);
}
