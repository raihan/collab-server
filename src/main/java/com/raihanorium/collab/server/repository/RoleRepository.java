package com.raihanorium.collab.server.repository;

import com.raihanorium.collab.server.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ataul.raihan on 10/14/2017.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
