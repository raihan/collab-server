package com.raihanorium.collab.server.repository;

import com.raihanorium.collab.server.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ataul.raihan on 9/17/2017.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    public User getByIdAndEnabled(Long id, boolean enabled);

    public User getByUsernameAndEnabled(String username, boolean enabled);

    public Page<User> findAllByEnabled(boolean enabled, Pageable pageable);

    public int countByEmailEquals(String email);

    public int countByUsernameEquals(String username);
}
