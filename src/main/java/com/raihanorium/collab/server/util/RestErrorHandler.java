package com.raihanorium.collab.server.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Locale;

/**
 * Created by ataul.raihan on 9/17/2017.
 */
@ControllerAdvice
public class RestErrorHandler {
    private MessageSource messageSource;
    private static final Logger logger = Logger.getLogger(RestErrorHandler.class);

    @Autowired
    Messages messages;

    @Autowired
    public RestErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestResponse processValidationError(MethodArgumentNotValidException ex) {
        logger.error(ex.getMessage(), ex);

        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();

        return processFieldErrors(fieldErrors);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public
    @ResponseBody
    RestResponse handleBadRequest(HttpMessageNotReadableException ex) {
        logger.error(ex.getMessage(), ex);

        RestResponse restResponse = new RestResponse();
        restResponse.setError(true);
        restResponse.addMessage(messages.getMessage("exception.bad.request"));

        return restResponse;
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public
    @ResponseBody
    RestResponse handleAccessDenied(HttpMessageNotReadableException ex) {
        logger.error(ex.getMessage(), ex);

        RestResponse restResponse = new RestResponse();
        restResponse.setError(true);
        restResponse.addMessage(messages.getMessage("exception.access.denied"));

        return restResponse;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public
    @ResponseBody
    RestResponse handleInternalError(Exception ex) {
        logger.error(ex.getMessage(), ex);

        RestResponse restResponse = new RestResponse();
        restResponse.setError(true);
        restResponse.addMessage(messages.getMessage("exception.internal.error"));

        return restResponse;
    }

    private RestResponse processFieldErrors(List<FieldError> fieldErrors) {
        RestResponse restResponse = new RestResponse();
        restResponse.setError(true);

        for (FieldError fieldError : fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            restResponse.getErrorFields().add(fieldError.getField());
            restResponse.getMessages().add(localizedErrorMessage);
        }

        return restResponse;
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale = LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);

        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }

        return localizedErrorMessage;
    }
}
