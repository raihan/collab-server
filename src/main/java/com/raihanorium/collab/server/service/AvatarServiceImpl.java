package com.raihanorium.collab.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.raihanorium.collab.server.util.Messages;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import java.util.Map;

/**
 * Created by ataul.raihan on 9/23/2017.
 */
public class AvatarServiceImpl implements AvatarService {
    @Autowired
    Messages messages;

    @Override
    public String getAvatarUrl(String email) {
        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        DefaultHttpClient client = new DefaultHttpClient();
        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        final String url = String.format(messages.getMessage("avatar.url"), email);
        HttpGet httpGet = new HttpGet(url);

        HttpResponse response = null;
        String imagePath = null;
        try {
            response = httpClient.execute(httpGet);
            ObjectMapper mapper = new ObjectMapper();
            Map jsonMap = mapper.readValue(response.getEntity().getContent(), Map.class);
            imagePath = jsonMap.get("Image").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imagePath;
    }
}
