package com.raihanorium.collab.server.service;

import com.raihanorium.collab.server.model.Role;
import com.raihanorium.collab.server.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by ataul.raihan on 9/17/2017.
 */
public interface UserService {
    public User add(User user);

    public User get(Long id);

    User get(String username);

    User get(String username, String password);

    public Page getAll(Pageable pageable);

    public Role getUserRole();

    public Role getAdminRole();

    public User getLoggedInUser();
}
