package com.raihanorium.collab.server.service;

import com.raihanorium.collab.server.model.Role;
import com.raihanorium.collab.server.model.User;
import com.raihanorium.collab.server.repository.RoleRepository;
import com.raihanorium.collab.server.repository.UserRepository;
import com.raihanorium.collab.server.util.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ataul.raihan on 9/17/2017.
 */
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    Messages messages;
    @Autowired
    AvatarService avatarService;
    @Autowired
    RoleRepository roleRepository;

    @Override
    public User add(User user) {
        if (isExistsEmail(user.getEmail())) throw new RuntimeException(messages.getMessage("email.already.exists"));
        if (isExistsUsername(user.getUsername()))
            throw new RuntimeException(messages.getMessage("username.already.exists"));

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
        user.setPassword(encoder.encode(user.getPassword()));

        user.setAvatar(avatarService.getAvatarUrl(user.getEmail()));

        user.getRoles().add(getUserRole());

        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(Long id) {
        return userRepository.getByIdAndEnabled(id, true);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(String username) {
        return userRepository.getByUsernameAndEnabled(username, true);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(String username, String password) {
        User user = userRepository.getByUsernameAndEnabled(username, true);
        if (user == null) return null;

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
        boolean passwordMatches = encoder.matches(password, user.getPassword());

        if (!passwordMatches) return null;

        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public Page getAll(Pageable pageable) {
        return userRepository.findAllByEnabled(true, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Role getUserRole() {
        return roleRepository.findOne(2L);
    }

    @Override
    @Transactional(readOnly = true)
    public Role getAdminRole() {
        return roleRepository.findOne(1L);
    }

    @Override
    public User getLoggedInUser() {
        org.springframework.security.core.userdetails.User springUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (springUser == null) {
            throw new UsernameNotFoundException("Not logged in");
        }
        return get(springUser.getUsername());
    }

    private boolean isExistsEmail(String email) {
        return userRepository.countByEmailEquals(email) > 0;
    }

    private boolean isExistsUsername(String username) {
        return userRepository.countByUsernameEquals(username) > 0;
    }
}
