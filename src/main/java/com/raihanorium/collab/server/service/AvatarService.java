package com.raihanorium.collab.server.service;

/**
 * Created by ataul.raihan on 9/23/2017.
 */
public interface AvatarService {
    public String getAvatarUrl(String email);
}
