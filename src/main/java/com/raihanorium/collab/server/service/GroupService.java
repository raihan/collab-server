package com.raihanorium.collab.server.service;

import com.raihanorium.collab.server.model.Group;
import com.raihanorium.collab.server.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author ataul.raihan
 * @date 10/21/2017
 */
public interface GroupService {
    public Page getAll(Pageable pageable);

    public Page getAll(User admin, Pageable pageable);

    public Group add(Group group);
}
