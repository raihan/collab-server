package com.raihanorium.collab.server.service;

import com.raihanorium.collab.server.model.Group;
import com.raihanorium.collab.server.model.User;
import com.raihanorium.collab.server.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.HashSet;
import java.util.Set;

/**
 * @author ataul.raihan
 * @date 10/21/2017
 */
public class GroupServiceImpl implements GroupService {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    UserService userService;

    @Override
    public Page getAll(Pageable pageable) {
        return groupRepository.findAllByEnabled(true, pageable);
    }

    @Override
    public Page getAll(User admin, Pageable pageable) {
        Set<User> admins = new HashSet<>();
        admins.add(admin);

        return groupRepository.findAllByEnabledAndAdminsContains(true, admins, pageable);
    }

    @Override
    public Group add(Group group) {
        User collabUser = userService.getLoggedInUser();
        group.setCreatedBy(collabUser.getId());
        group.getAdmins().add(collabUser);
        group.getMembers().add(collabUser);

        return groupRepository.save(group);
    }
}
