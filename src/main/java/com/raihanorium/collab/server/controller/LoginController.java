package com.raihanorium.collab.server.controller;

import com.raihanorium.collab.server.model.User;
import com.raihanorium.collab.server.service.UserService;
import com.raihanorium.collab.server.util.Messages;
import com.raihanorium.collab.server.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ataul.raihan on 9/29/2017.
 */
@RestController
@RequestMapping(value = "/login")
public class LoginController {
    @Autowired
    UserService userService;
    @Autowired
    Messages messages;

    @RequestMapping(method = RequestMethod.POST)
    public RestResponse login(@RequestParam String username, @RequestParam String password) {
        RestResponse restResponse = new RestResponse();
        try {
            User user = userService.get(username, password);
            if(user == null) {
                throw new BadCredentialsException(messages.getMessage("login.failed"));
            }
            restResponse.setData(user);
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }
}
