package com.raihanorium.collab.server.controller;

import com.raihanorium.collab.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ataul.raihan on 9/16/2017.
 */

@RestController
@RequestMapping
public class HomeController {
    @Autowired
    UserService userService;

    @RequestMapping
    public String showHome() {
        return "<h3>Test API v1.01</h3>";
    }
}
