package com.raihanorium.collab.server.controller;

import com.raihanorium.collab.server.model.Group;
import com.raihanorium.collab.server.service.GroupService;
import com.raihanorium.collab.server.service.UserService;
import com.raihanorium.collab.server.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ataul.raihan
 */

@RestController
@RequestMapping(value = "/group")
@PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
public class GroupController {
    @Autowired
    GroupService groupService;
    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    public RestResponse getAll(Pageable pageable) {
        RestResponse restResponse = new RestResponse();
        try {
            restResponse.setData(groupService.getAll(pageable));
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }

    @RequestMapping(value = "/my", method = RequestMethod.GET)
    @PreAuthorize(value = "hasRole('ROLE_USER')")
    public RestResponse getAllMyGroups(Pageable pageable) {
        RestResponse restResponse = new RestResponse();
        try {
            restResponse.setData(groupService.getAll(userService.getLoggedInUser(), pageable));
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public RestResponse create(@Validated @RequestBody Group group) {
        RestResponse restResponse = new RestResponse();
        try {
            restResponse.setData(groupService.add(group));
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }
}
