package com.raihanorium.collab.server.controller;

import com.raihanorium.collab.server.model.User;
import com.raihanorium.collab.server.service.UserService;
import com.raihanorium.collab.server.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ataul.raihan on 9/17/2017.
 */
@RestController
@RequestMapping(value = "/user")
@PreAuthorize(value = "hasRole('ROLE_ADMIN')")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public RestResponse getAll(Pageable pageable) {
        RestResponse restResponse = new RestResponse();
        try {
            restResponse.setData(userService.getAll(pageable));
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public RestResponse get(@PathVariable("id") Long id) {
        RestResponse restResponse = new RestResponse();
        try {
            restResponse.setData(userService.get(id));
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize(value = "isAnonymous()")
    public RestResponse create(@Validated @RequestBody User user) {
        RestResponse restResponse = new RestResponse();
        try {
            restResponse.setData(userService.add(user));
        } catch (Exception e) {
            restResponse.setError(true);
            restResponse.addMessage(e.getMessage());
        }
        return restResponse;
    }
}
