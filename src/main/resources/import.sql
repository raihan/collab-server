-- Roles
INSERT INTO role (id, code, label) VALUES (1, 'ROLE_ADMIN', 'Admin');
INSERT INTO role (id, code, label) VALUES (2, 'ROLE_USER', 'User');

-- Users (Password is 'password')
INSERT INTO collab_user (id, avatar, created_on, credentialsexpired, email, enabled, expired, full_name, locked, password, username) VALUES (1, null, '2017-09-29 21:12:01.566000', false, 'admin@collab.com', true, false, 'Administrator', false, '$2a$06$71XIpzmwEXpkvUxmvnlzquEXqz3E7FZZBFxPth.lqHvNn9eyDo3QW', 'admin');
INSERT INTO collab_user (id, avatar, created_on, credentialsexpired, email, enabled, expired, full_name, locked, password, username) VALUES (2, null, '2017-09-29 21:12:01.566000', false, 'user@collab.com', true, false, 'User', false, '$2a$06$71XIpzmwEXpkvUxmvnlzquEXqz3E7FZZBFxPth.lqHvNn9eyDo3QW', 'user');

-- Roles of users
INSERT INTO user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO user_role (user_id, role_id) VALUES (1, 2);
INSERT INTO user_role (user_id, role_id) VALUES (2, 2);