Features so far
====================================================================

- User can create one or more team
- User can join one or more team(needs approval of the admin of that team)
- User can call meetings (select participants, define location)
- Meetings can take place online (?)
- User can message other user/group of the same company
- User can organize meetings, todos, contacts, reminders
- User can call other user to meet physically immediately by one click
